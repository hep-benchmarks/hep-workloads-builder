#!/bin/bash

function myecho() {
  echo -e "[${FUNCNAME[1]}] $@"
}

function fail() {
  myecho "\n------------------------\nFailing '$@'\n------------------------\n" >&2
  myecho "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  myecho "\n finished (NOT OK) at $(date)\n"
  myecho "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
  exit 1
}

# Create a spec from the cvmfs trace file, then run shrinkwrap to create a cvmfs export
# ${CVMFS_EXPORT_DIR}/cvmfs: directory where the cvmfs export should be stored
# ${CVMFS_EXPORT_DIR}/traces: host directory where the cvmfs config should be stored
# ${CVMFS_TRACES_DIR}: directory containing the cvmfs trace files
function run_shrinkwrap() {
  myecho "................................................"
  myecho " starting at $(date)"
  myecho "current directory is $(pwd)"
  myecho "CVMFS_EXPORT_DIR $CVMFS_EXPORT_DIR"
  if [ -e ${CVMFS_EXPORT_DIR}/cvmfs ]; then
    myecho "${CVMFS_EXPORT_DIR} already exists: will remove it"
    ###echo "${CVMFS_EXPORT_DIR} already exists: remove it? (y/[n]) "
    ###read myansw
    ###if [ "$myansw" == "n" ]; then return 1; fi
    rm -rf ${CVMFS_EXPORT_DIR}/cvmfs
  fi
  ls -l ${CVMFS_EXPORT_DIR}
  mkdir -p ${CVMFS_EXPORT_DIR}/cvmfs
  [ ! -e ${CVMFS_EXPORT_DIR}/traces ] && mkdir -p ${CVMFS_EXPORT_DIR}/traces
  
  cvmfs_shrink_conf=$CVMFS_EXPORT_DIR/traces/generic_config.cern.ch.config # no need to export this
  cat >${cvmfs_shrink_conf} <<EOF
CVMFS_CACHE_BASE=/var/lib/cvmfs/shrinkwrap
CVMFS_HTTP_PROXY=DIRECT
CVMFS_KEYS_DIR=/etc/cvmfs/keys/cern.ch # from /etc/cvmfs/domain.d/cern.ch.conf
CVMFS_MOUNT_DIR=/cvmfs # from /etc/cvmfs/default.conf
CVMFS_SERVER_URL='http://cvmfs-stratum-zero-hpc.cern.ch/cvmfs/@fqrn@' # from /etc/cvmfs/domain.d/cern.ch.conf
CVMFS_SHARED_CACHE=no
export CMS_LOCAL_SITE=T0_CH_CERN
EOF

  for acvmfs in $(ls ${CVMFS_TRACES_DIR} | grep "\.log"); do
    myecho "... shrinking  " $acvmfs
    specname=$(echo $acvmfs | sed -e 's@trace\.log@spec\.txt@')
    reponame=$(echo $acvmfs | sed -e 's@cvmfs-\([^\.]*\)\.cern\.ch.*@\1\.cern.ch@')
    myecho "specname $specname"
    myecho "reponame $reponame"
    date

    cvmfs_talk -i $reponame tracebuffer flush
    myecho "python /usr/libexec/cvmfs/shrinkwrap/spec_builder.py --policy=exact ${CVMFS_TRACES_DIR}/$acvmfs ${CVMFS_TRACES_DIR}/$specname"
    if ! python /usr/libexec/cvmfs/shrinkwrap/spec_builder.py --policy=exact ${CVMFS_TRACES_DIR}/$acvmfs ${CVMFS_TRACES_DIR}/$specname; then
      fail "spec_builder.py" # fix BMK-136 (spec_builder was silently failing, leading to missing cvmfs files)
    fi

    append_custom_paths

    myecho "cvmfs_shrinkwrap --repo $reponame --src-config ${cvmfs_shrink_conf} --spec-file ${CVMFS_TRACES_DIR}/$specname --dest-base ${CVMFS_EXPORT_DIR}/cvmfs/ -j 4"
    cvmfs_shrinkwrap --repo $reponame --src-config ${cvmfs_shrink_conf} --spec-file ${CVMFS_TRACES_DIR}/$specname --dest-base ${CVMFS_EXPORT_DIR}/cvmfs/ -j 4 || fail "cvmfs_shrinkwrap failed"
    date
  done

  export_traces

  myecho "finished at $(date)"
  return 0
}

function export_traces() {
  myecho "saving $specname on ${CVMFS_EXPORT_DIR/traces}/$specname"
  [ ! -e ${CVMFS_EXPORT_DIR}/traces ] && mkdir -p ${CVMFS_EXPORT_DIR}/traces
  cp ${CVMFS_TRACES_DIR}/$specname ${CVMFS_EXPORT_DIR}/traces
  date
}

# FIXME: review how to append custom paths
function append_custom_paths() {
  myecho "checking custom specfile for $reponame"
  append_file=${CVMFS_EXPORT_DIR}/traces/${reponame}_spec_custom.txt
  if [ -e ${append_file} ]; then
    myecho "appending custom paths to ${CVMFS_TRACES_DIR}/$specname based on ${append_file}"
    echo " " >>${CVMFS_TRACES_DIR}/$specname
    cat ${append_file} >>${CVMFS_TRACES_DIR}/$specname
  else
    myecho "no custom specfile found, continue as usual"
  fi
}

function usage() {
  myecho "Usage: $SCRIPT [OPTIONS]

    OPTIONS:
    -h
    \t Display this help and exit
    -d
    \t Debug verbosity
    -t <string>
    \t Set environment variable CVMFS_TRACES_DIR: directory containing the cvmfs trace files
    -e <string>
    \t Set environment variable CVMFS_EXPORT_DIR: root directory where the subdir ./cvmfs and ./traces export are stored
    \t If traces have to be extended, the dir CVMFS_EXPORT_DIR/traces should contain a file of name reponame_spec_custom.txt (such as cms.cern.ch_spec_custom.txt)
"
}

SOURCEDIR=$(readlink -f $(dirname $0))
SCRIPT=$(readlink -f $0)

# Input environment variable ${CVMFS_TRACES_DIR}: directory containing the cvmfs trace files
# Input environment variable ${CVMFS_EXPORT_DIR}: directory where the cvmfs export should be stored

export CVMFS_TRACES_DIR=
export CVMFS_EXPORT_DIR=

while getopts "hdt:e:" flag; do
  case $flag in
  t) CVMFS_TRACES_DIR=$OPTARG ;;
  e) CVMFS_EXPORT_DIR=$OPTARG ;;
  d) DEBUG=1 ;;
  h)
    usage
    EXIT_STATUS=0
    exit ${EXIT_STATUS}
    ;;
  *)
    usage
    EXIT_STATUS=1
    exit ${EXIT_STATUS}
    ;;
  esac
done

for var in CVMFS_TRACES_DIR CVMFS_EXPORT_DIR; do
  if [[ -z ${!var} ]]; then
    myecho "ERROR variable ${var} is not defined"
    usage
    exit 1
  else
    myecho "variable ${var} is ${!var}"
  fi
done

run_shrinkwrap || fail '[shrinkwrap.sh]'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\n[shrinkwap.sh] finished (OK) at $(date)\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
