#!/bin/bash

function onEXIT {
    unmount_cvmfs
}
trap onEXIT EXIT


function fail(){
  echo -e "\n------------------------\nFailing '$@'\n------------------------\n" >&2
  echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  echo -e "\n[main.sh] finished (NOT OK) at $(date)\n"
  echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
  exit 1
}

# Mount the cvmfs repos specified in $HEPWL_CVMFSREPOS
# Input environment variable ${HEPWL_CVMFSREPOS}: cvmfs repos to mount
# Input environment variable ${MAIN_CVMFSTRACESDIR}: directory containing the cvmfs trace files
function mount_cvmfs(){
  echo "[mount_cvmfs] ................................................"
  echo "[mount_cvmfs] starting at $(date)"
  echo "[mount_cvmfs] current directory is $(pwd)"
  echo "NB: it's fine if there is a single error message such as 'Failed to get D-Bus connection: Operation not permitted'"
  if [ ! -e $MAIN_CVMFSTRACESDIR ]; then
    mkdir -p $MAIN_CVMFSTRACESDIR || fail "[mount_cvmfs] cannot create $MAIN_CVMFSTRACESDIR"
    chown cvmfs $MAIN_CVMFSTRACESDIR || fail "[mount_cvmfs] cannot chown $MAIN_CVMFSTRACESDIR"
  fi
  cat > /etc/cvmfs/default.local <<EOF
CVMFS_REPOSITORIES=${HEPWL_CVMFSREPOS}
CVMFS_QUOTA_LIMIT=6000
CVMFS_CACHE_BASE=/scratch/cache/cvmfs2
CVMFS_MOUNT_RW=yes
CVMFS_HTTP_PROXY="http://squid.cern.ch:8060|http://ca-proxy.cern.ch:3128;DIRECT"
CVMFS_TRACEFILE=${MAIN_CVMFSTRACESDIR}/cvmfs-@fqrn@.trace.log
EOF

  mkdir -p  /etc/cvmfs/config.d
  echo "export CMS_LOCAL_SITE=/cvmfs/cms.cern.ch/SITECONF/T0_CH_CERN" > /etc/cvmfs/config.d/cms.cern.ch.local

  # NEW OPTION 1 (BMK-145) - main.sh as a subprocess
  #if [ -e /cvmfs ]; then fail "/cvmfs already exists"; fi # 1-inception
  #ln -sf $CIENV_CVMFSVOLUME /cvmfs # replaces "docker run -v $CIENV_CVMFSVOLUME:/cvmfs:shared"? IT DOES NOT WORK...
  #if [ ! -d /cvmfs ]; then fail "/cvmfs does not exist"; fi # assume that /cvmfs has been created by now

  # OLD OPTION 2 (BMK-145) - main.sh as an entrypoint of docker run
  [ -d /cvmfs ] || fail "[mount_cvmfs] /cvmfs does not exist" # assume that /cvmfs has been created by now (e.g. by docker run -v $CIENV_CVMFSVOLUME:/cvmfs:shared)

  cvmfs_config setup nostart || fail "[mount_cvmfs] problem with cvmfs_config setup nostart" # this would create /cvmfs if it did not exist yet
  for repo in `echo ${HEPWL_CVMFSREPOS}| sed -e 's@,@ @g'`; do
    if [[ -e /cvmfs/$repo ]]; then
      umount /cvmfs/$repo # OPTION 2 ONLY - NEEDED AT ALL?
      rm -rf /cvmfs/$repo # OPTION 2 ONLY - NEEDED AT ALL?
    fi
    mkdir /cvmfs/$repo # Assume that /cvmfs has been created by now
    mount -t cvmfs $repo /cvmfs/$repo
    echo "[mount_cvmfs] ls -l /cvmfs/$repo"
    ls -l /cvmfs/$repo
  done
  echo "[mount_cvmfs] finished at $(date)"
  return 0
}


# Unmount the cvmfs repos specified in $HEPWL_CVMFSREPOS
# Input environment variable ${HEPWL_CVMFSREPOS}: cvmfs repos to unmount
function unmount_cvmfs(){
  echo "[unmount_cvmfs] ................................................"
  echo "[unmount_cvmfs] starting at $(date)"
  echo "[unmount_cvmfs] current directory is $(pwd)"
  if [ "$HEPWL_CVMFSREPOS" == "" ]; then fail "HEPWL_CVMFSREPOS is not set"; fi
  echo "[unmount_cvmfs] unmounting cvmfs repositories $HEPWL_CVMFSREPOS"
  for repo in `echo ${HEPWL_CVMFSREPOS}| sed -e 's@,@ @g'`; do
    umount /cvmfs/$repo || echo "WARNING! Could not umount /cvmfs/$repo"
    rm -rf /cvmfs/$repo || echo "WARNING! Could not remove /cvmfs/$repo"
  done
  echo "[unmount_cvmfs] finished at $(date)"
  return 0
}




function usage(){
    echo -e "Usage: $SCRIPT [OPTIONS]

    OPTIONS:
    -h
    \t Display this help and exit
    -d
    \t Debug verbosity
    -r <string> 
    \t set environment variable HEPWL_CVMFSREPOS: cvmfs repo list comma separated
    -t <string> 
    \t set environment variable MAIN_CVMFSTRACESDIR: directory containing the cvmfs trace files
  "
}


SOURCEDIR=$(readlink -f $(dirname $0))
SCRIPT=$(readlink -f $0)

# Default values
HEPWL_CVMFSREPOS=
MAIN_CVMFSTRACESDIR=

while getopts "hdr:t:" flag; do
    case $flag in
    r) HEPWL_CVMFSREPOS=$OPTARG ;;
    t) MAIN_CVMFSTRACESDIR=$OPTARG ;;
    d) DEBUG=1 ;;
    h)
        usage
        EXIT_STATUS=0
        exit ${EXIT_STATUS}
        ;;
    *)
        usage
        EXIT_STATUS=1
        exit ${EXIT_STATUS}
        ;;
    esac
done


for var in HEPWL_CVMFSREPOS MAIN_CVMFSTRACESDIR; do
  if [[ -z ${!var} ]]; then 
    echo "ERROR variable ${var} is not defined"
    usage
    exit 1
  fi
done


export HEPWL_CVMFSREPOS
export MAIN_CVMFSTRACESDIR

mount_cvmfs || fail '[main.sh] mount_cvmfs'
echo -e "\nStay alive to expose cvmfs"

cvmfs_config probe
STATUS=$?
while [[ $STATUS -eq 0 ]]; do
  sleep 10m
  cvmfs_config probe
  STATUS=$? 
done
unmount_cvmfs || fail '[main.sh] unmount_cvmfs'

echo -e "\n@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
echo -e "\n[main.sh] finished (OK) at $(date)\n"
echo -e "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@\n"
