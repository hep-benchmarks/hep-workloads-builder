#!/bin/bash
subject=$1
announcement=$2
echo "subject: " $subject
echo "Announcement file: " $announcement
postfix start
cat $announcement
cat $announcement | mail -r ${CI_MAIL_FROM} -s "$subject" ${CI_ANNOUNCE_TO}
sleep 100s # keep the container alive, otherwise no email is sent (emails are sent only once per minute, see BMK-80)
