stages:
    - build-images
    - test

############################################################
#####              BUILD IMAGES                        #####
############################################################

#include: './templates/template_build_image.yml'

.definition_build_image: &template_build_image
    image: # NB enable shared runners and do not specify a CI tag
        name: gitlab-registry.cern.ch/ci-tools/docker-image-builder # CERN version of the Kaniko image
        entrypoint: [""]
    script:
        - echo "current commit is ${CI_COMMIT_SHA:0:8}"
        - echo "current branch is ${CI_COMMIT_BRANCH}"
        - echo "current tag is ${CI_COMMIT_TAG}"
        - if [[ -z $DOCKERFILE ]]; then echo "ERROR variable DOCKERFILE is not defined "; exit 1; fi 
        - if [[ -z $CONTEXT ]]; then echo "ERROR variable CONTEXT is not defined "; exit 1; fi 
        - if [[ -z $IMAGE_NAME ]]; then echo "ERROR variable IMAGE_NAME is not defined "; exit 1; fi 
        - if [[ -z $IMAGE_TAG ]]; then echo "ERROR variable IMAGE_TAG is not defined "; exit 1; fi 
        - export DESTINATIONS="--destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:$IMAGE_TAG --destination $CI_REGISTRY_IMAGE/$IMAGE_NAME:ci-${CI_COMMIT_SHORT_SHA}"
        - echo "DESTINATIONS $DESTINATIONS"
        # Prepare Kaniko configuration file
        - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
        # Build and push the image from the Dockerfile at the root of the project.
        # To push to a specific docker tag, amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
        # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
        - /kaniko/executor --context $CONTEXT --dockerfile $DOCKERFILE $DESTINATIONS


###########################################################
# docker in docker image: to trigger other docker runs
###########################################################

job_build_dind_image: 
    stage: build-images
    before_script:
        - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/dind/Dockerfile
        - export CONTEXT=$CI_PROJECT_DIR/docker-images/dind
        - export IMAGE_NAME=dind
        - export IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
    <<: *template_build_image
    only:
        variables:
            - $CI_COMMIT_BRANCH =~ /^qa.*$/
            - $CI_COMMIT_TAG =~ /^v.*$/
        changes:
           - docker-images/dind/*

###########################################################
# Announcement image: simple mail server to send a message
###########################################################

job_build_announcement_image_qa: &build_announcement_image_qa
    stage: build-images
    before_script:
        - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/announcement/Dockerfile
        - export CONTEXT=$CI_PROJECT_DIR/docker-images/announcement
        - export IMAGE_NAME=announcement
        - export IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
    <<: *template_build_image
    only:
        variables:
           - $CI_COMMIT_BRANCH =~ /^qa.*$/
           - $CI_COMMIT_TAG =~ /^v.*$/
        changes:
           - docker-images/announcement/*

job_test_announcement_image:
    stage: test
    image: # NB enable shared runners and do not specify a CI tag
        name: $CI_REGISTRY_IMAGE/announcement:v1.0
    script: 
        - export IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
        - export IMAGE=thisisatest_$IMAGE_TAG
        - export subject="New Docker container available $IMAGE"
        - export announcement="/tmp/announce.txt"
        - |
            cat > $announcement << EOF
            Dear, 
            we are pleased to inform that a new version has been released for the container image
            
            ${IMAGE}
            
            COMMIT DESCRIPTION $CI_COMMIT_DESCRIPTION

            Please DO NOT REPLY
            Report automatically generated from GitLab CI in pipeline ${CI_PIPELINE_URL}
            [$(date)]

            Yours sincerely,
            HEPiX Benchmarking Working Group
            EOF       
        - /announce.sh "$subject" $announcement

    only:
        variables:
            - $CI_COMMIT_BRANCH =~ /^qa.*$/
            - $CI_COMMIT_TAG =~ /^v.*$/
        changes:
           - docker-images/announcement/*

###########################################################
# CVMFS image: docker container deploying cvmfs
###########################################################

job_build_cvmfs_image: 
    stage: build-images
    before_script:
        - export DOCKERFILE=$CI_PROJECT_DIR/docker-images/cvmfs/Dockerfile
        - export CONTEXT=$CI_PROJECT_DIR/docker-images/cvmfs
        - export IMAGE_NAME=cvmfs-image
        - export IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
    <<: *template_build_image
    only:
        variables:
            - $CI_COMMIT_BRANCH =~ /^qa.*$/
            - $CI_COMMIT_TAG =~ /^v.*$/
        changes:
            - docker-images/cvmfs/*

job_test_cvmfs_image:
    stage: test
    image: gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-builder/dind:qa
    tags:
        - hep-workload-gpu-docker-builder
    before_script:
        - export CIENV_CVMFSVOLUME=/scratch/cvmfs_hep/CI-JOB-${CI_JOB_ID}
        - export CVMFS_EXPORT_DIR=/scratch/export_cvmfs/CI-JOB-${CI_JOB_ID}
        - export CVMFS_IMAGE=gitlab-registry.cern.ch/hep-benchmarks/hep-workloads-builder/cvmfs-image:${CI_COMMIT_TAG:-$CI_COMMIT_BRANCH}
        - docker pull ${CVMFS_IMAGE}
        - docker run --name cvmfs_${CI_JOB_ID} -d --privileged -v ${CVMFS_EXPORT_DIR}:${CVMFS_EXPORT_DIR} -v ${CIENV_CVMFSVOLUME}:/cvmfs:shared ${CVMFS_IMAGE} -r cms.cern.ch -t /tmp/traces
    script: 
        - sleep 1m # to give time to cvmfs to start
        - export CIENV_CVMFSVOLUME=/scratch/cvmfs_hep/CI-JOB-${CI_JOB_ID}
        - export CVMFS_EXPORT_DIR=/scratch/export_cvmfs/CI-JOB-${CI_JOB_ID}
        # check cvmfs is running
        - docker exec cvmfs_${CI_JOB_ID} cvmfs_config probe
        # access some file in CVMFS to trigger traces
        - docker run --rm -v ${CIENV_CVMFSVOLUME}:/cvmfs busybox ls /cvmfs/cms.cern.ch
        - docker run --rm -v ${CIENV_CVMFSVOLUME}:/cvmfs busybox cat /cvmfs/cms.cern.ch/SITECONF/local/JobConfig/site-local-config.xml
        # force flush 
        - docker exec cvmfs_${CI_JOB_ID} cvmfs_talk -i cms.cern.ch tracebuffer flush
        # extract the list of accessed files
        - docker exec cvmfs_${CI_JOB_ID} python /usr/libexec/cvmfs/shrinkwrap/spec_builder.py --policy=exact /tmp/traces/cvmfs-cms.cern.ch.trace.log /tmp/traces/cvmfs-cms.cern.ch.spec
        - docker cp cvmfs_${CI_JOB_ID}:/tmp/traces ${CI_PROJECT_DIR}/traces
        # test shrinkwrapper
        - docker exec cvmfs_${CI_JOB_ID} /root/shrinkwrap.sh -t /tmp/traces/ -e ${CVMFS_EXPORT_DIR}
        - rm -rf ${CVMFS_EXPORT_DIR}/cvmfs/.data
        - ls -R ${CVMFS_EXPORT_DIR} > ${CI_PROJECT_DIR}/cvmfs_export_dir_content
        - mv ${CVMFS_EXPORT_DIR}/cvmfs ${CI_PROJECT_DIR}
    after_script:
        - docker rm -f cvmfs_${CI_JOB_ID}
    only:
        variables:
            - $CI_COMMIT_BRANCH =~ /^qa.*$/
            - $CI_COMMIT_TAG =~ /^v.*$/
        #changes:
        #- docker-images/cvmfs/*
    artifacts:
        paths:
            - ${CI_PROJECT_DIR}/traces
            - ${CI_PROJECT_DIR}/cvmfs_export_dir_content
            - ${CI_PROJECT_DIR}/cvmfs
        expire_in: 1 week
        when: always

